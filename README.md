Role Scipion
=========

Install and configure Scipion software for all users on ubuntu installation

Compatiblity
------------

This role can be used only on Ubuntu 18 LTS Other OS will come later.

Role Variables Used
--------------

You can find in defaults/main.yml all variables used in tasks



| Variable          | Default Value                                        | Type   | Description                                        |
| ----------------- | :--------------------------------------------------- | :----- | -------------------------------------------------- |
| dl_url            | "https://github.com/I2PC/scipion/archive/master.zip" | String | Scipion version to install                         |
| scipion_dest      | /usr/local/scipion                                   | String | Path Scipion installation                          |
| scipion_installer | scipion-master.zip                                   | String | Scipion zip to get the Scipion installer           |
| scipion_zip       | "/tmp/{{ scipion_installer }}"                       | String | Path to copy the Scpion.zip file                   |
| scipion_conf      | /usr/local/scipion/scipion-master/config/            | String | Copying config file for scipion installation       |
| scipion_conf_root | /root/.config/scipion                                | String | Path config file for root installation             |
| scipion_profile_d | /etc/profile.d/scipion_conf_users.sh                 | String | Scipion config file loading for  multiusers users  |
| installed_path    | /usr/local/scipion/scipion-master/scipion            | String | Path to Scipion installation folder                |
| dl_urs_git        | "https://github.com/I2PC/xmipp.git"                  | String | git for xmipp pluging                              |
| scipion_xmipp     | /usr/local/scipion/scipion-master                    | String | Path installation xmipp pluging                    |
| scipion_profle_d  | /etc/profile.d                                       | String | Store the script loading confif file for all users |


